
/*
 * 
 */
kore.google = kore.google || {};

kore.google.maps = function() {
    "use strict";

    var brandName = "Jozack Recruitment Ltd.",
        brandEmail = "info@jozackrecruitment.co.uk",
        brandPhoneNumber = "00447760942873",
        brandDisplayNumber = "(+44) 7760 942873",
        brandAddress = "",
        mandrillKey = "9Xq5QQNEhEmYnXFr4b7QXw",

        initializeGoogleMap = function () {

          var mapContainer = $('#GoogleMap'),
              latlng = new google.maps.LatLng(51.527321, -0.088670),
              mapOptions = {
                zoom: 15,
                center: latlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
              },
              map = new google.maps.Map(mapContainer, mapOptions),
              marker = new google.maps.Marker({
                  position: latlng, 
                  map: map,
                  title: brandName
              }),
              contentString = '<div id="content">'+
                                '<h4 id="firstHeading" class="firstHeading">'+ brandName +'</h4>'+
                                '<div id="bodyContent">'+
                                  '<p>'+ brandAddress +'</p>'+
                                  '<p><span class="fa fa-phone"></span>&nbsp;<a href="callto://+'+ brandPhoneNumber +'"><span class="s_hidden">'+ brandDisplayNumber +'</span></a></p>'+
                                  '<p><span class="fa fa-envelope"></span>&nbsp;<a href="mailto:'+ brandEmail +'">'+ brandEmail +'</p>'+
                                '</div>'+
                              '</div>',
            infowindow = new google.maps.InfoWindow({
                content: contentString
            });

            google.maps.event.addListener(marker, 'click', function() {
              infowindow.open(map,marker);
            });
        },

        createEmail = function (model){

          var message = "<p>"+ model.message.replace(/[\n\r]/, '</p><p>') +"</p>",
              emailModel = {
                "key": mandrillKey,
                "message": {
                    "html": message,
                    "subject": model.subject,
                    "from_email": model.email,
                    "from_name": model.fullName,
                    "to": [
                        {
                            "email": brandEmail,
                            "name": "Olumuyiwa Raji",
                            "type": "to"
                        }
                    ],
                    "headers": {
                        "Reply-To": model.email
                    },
                    "important": false
                  }
              };

          return emailModel;
        },

        sendEmailUsingMandrillApi = function (scope){

          /*
          To make use of it, add this lines in index.html head section.
          <!-- Mandrill -->
          <script type="text/javascript" src="https://mandrillapp.com/api/docs/js/mandrill.js"></script>
          */

          var $mandrill = new mandrill.Mandrill(mandrillKey);
          $mandrill
            .messages
            .send(data, 
              function(response) {
                scope.success = true;
              }, 
              function(error) {
                scope.error = true;
              }
            );
        };

    return {

      sendEmail: function(scope, form) {
        // check to make sure the form is completely valid
        if (form.contactForm.$valid) {
            
            scope.requested = true;
            var data = createEmail(form.contact);
            
            $http({
              method  : 'POST',
              url     : 'https://mandrillapp.com/api/1.0/messages/send.json',
              data    : data,  
              headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
            })
            .success(function(data) {
              scope.success = true;
            })
            .error(function (data){
              scope.error = true;
            })
            .finally(function (){
              scope.requested = false;
            });
          }
      },

      showGoogleMap: function (){
        initializeGoogleMap();
      }
    };
} ();