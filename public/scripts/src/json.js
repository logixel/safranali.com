
/*
 * 
 */
kore.json = function () {
    "use strict";

    var _log = kore.Log,
        _isNull = kore.isNull;

    return  {
        parse: function (data) {

            /// <summary>Converts JSON string to a Javascript object, if it's a valid json string.</summary>
            /// <param name="data" type="string">A json formatted string.</param>
            /// <returns type="object">Converted json object.</returns>
            var result = "";
            try {
                result = JSON.parse(data);
            } catch (event) {
                
                //_log.debugFormat("ArgumentException: Invalid or empty JSON data. {0}", event);
            }
            return result;
        },

        stringify: function (obj) {

            /// <summary>Converts Javascript object to JSON string, if it's a valid object.</summary>
            /// <param name="obj" type="object">An object.</param>
            /// <returns type="string">Converted json string.</returns>
            var result = "";
            try {                
                result = JSON.stringify(obj);
            } catch (event) {
                
                //_log.debugFormat("ArgumentException: Invalid or empty object. {0}", event);
            }

            return result;
        },

        toUTCDate: function (data) {

            /// <summary>Converts JSON returned date in to UTC formatted date string.</summary>
            /// <param name="data" type="string">A json formatted date string.</param>
            /// <returns type="string">UTC converted date.</returns>
            var convertedDate = "";
            if (!_isNull(data)) {
                convertedDate = new Date(+data.replace(/\/Date\((-?\d+)\)\//gi, "$1"));

                var timestamp = Date.parse(convertedDate.getTime());
                if (_isNull(timestamp)) {
                    convertedDate = (new Date()).getTime();
                }
            }
            return convertedDate;
        }
    };
} ();