
/*
 * 
 */
kore.mandrill = function() {
    "use strict";

    var _ajax = kore.ajax,
        _isNull = kore.isNull,
        _utils = kore.utils,
        _contactForm = null,
        recipientName = "",
        brandName = "safranali",
        brandEmail = ["info@safranali.com", "safranali@gmail.com"],
        brandPhoneNumber = "00447872490696",
        brandDisplayNumber = "(+44)7872 490696",
        brandAddress = "",
        mandrillKey = "vuEkxe83FFPZBwcpdEBOrg",

        /*
         * 
         */
        _createEmail = function (model){

            var _message = "<p>"+ model.message.replace(/[\n\r]/, '</p><p>') +"</p>",
                _model = {
                    "key": mandrillKey,
                    "message": {
                        "html": _message,
                        "subject": model.subject,
                        "from_email": model.email,
                        "from_name": model.fullName,
                        "to": [
                            {
                                "email": brandEmail[0],
                                "name": "Safran Ali",
                                "type": "to"
                            },
                            {
                                "email": brandEmail[1],
                                "name": "Safran Ali",
                                "type": "to"
                            }
                        ],
                        "headers": {
                            "Reply-To": model.email
                        },
                        "important": false
                    }
                };

            return _model;
        },

        /*
         * 
         */
        _formatMessage = function(message, fullName, resumeUrl) {

            message = "Dear Safran, <br/><br/>" + message;

            if(_isNull(resumeUrl) === false) {
                message += "<br/><br/>Download resume <a href='"+ resumeUrl +"'>here</a>.";
            }

            message += "<br/><br/>Regards,<br/>";
            message += "<strong>"+ fullName +"</strong>";

            return message;
        },

        /*
         * 
         */
        _sendEmail = function() {

            // check to make sure the form is completely valid
            var _fullName = _contactForm.Lookup(".fullName"),
                _email = _contactForm.Lookup(".email"),
                _message = _contactForm.Lookup(".message"),
                _queryType = _contactForm.Lookup(".queryType"),
                _resumeUrl = _contactForm.Lookup(".resumeUrl"),
                _sectionContact = _contactForm.parent(),
                _requestUrl = "https://mandrillapp.com/api/1.0/messages/send.json";

            if(_isNull(_fullName) ||
               _isNull(_email) ||
               _isNull(_message)) {

                _sectionContact.Get(".msgError").removeClass("hidden");
                return;
            }

            _sectionContact.Get(".msgError").addClass("hidden");
            _contactForm.addClass("hidden");
            _sectionContact.Get(".msgLoading").removeClass("hidden");

            _message = _formatMessage(_message, _fullName, _resumeUrl);

            var _data = _createEmail({
                            message: _message,
                            subject: "["+ _utils.currentDate() +"] safranali.com query from " + _fullName,
                            fullName: _fullName,
                            email: _email
                        }),
                _model = {

                    caller: _sectionContact,
                    message: _message
                };

            _ajax.setContentType("application/x-www-form-urlencoded");
            _ajax.post("Json", _model, _requestUrl, _data, _sendEmailDone);
        },

        /*
         * 
         */
        _sendEmailDone = function(response) {

            var _model = response.model,
                _sectionContact = $(_model.caller),
                _message = _sectionContact.Get(".msgSuccess");

            if(response.success === false) {

                _message = _sectionContact.Get(".msgError");
            } else{

            }

            _sectionContact.Get(".msgLoading").addClass("hidden");
            _message.removeClass("hidden");
        },

        /*
         * 
         */
        _bindEvent = function() {

            var _sendBtn = _contactForm.Get(".btnSubmit");

            if(_isNull(_sendBtn) === false) {
                _sendBtn.on("click", function() {
                    _sendEmail();
                });
            }
        };

    return {

        /*
        * 
        */
        init: function(args) {

            var _getElement = kore.getElement;

            _contactForm = _getElement(args.formId);

            if(_isNull(args) || _isNull(args.formId) || _isNull(_contactForm)) {
                return;
            }

            _bindEvent();
        } 
    };
}();