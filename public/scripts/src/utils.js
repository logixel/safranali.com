
/*
 * 
 */
kore.utils = function () {
    "use strict";

    return {

        /// <summary></summary>
        /// <param name="" type=""></param>
        /// <returns type=""></returns>
        queryString: function(name){

            name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");

            var regexS = "[\\?&]"+name+"=([^&#]*)",
                regex = new RegExp( regexS ),
                results = regex.exec( window.location.href );

            if(results === null) {
                return "";
            }
            else{
                return decodeURIComponent(results[1].replace(/\+/g, " "));
            }
        },
        
        /// <summary>Is the string a guid</summary>
        /// <param name="" type=""></param>
        /// <returns type=""></returns>
        isGuid: function (str) {

            return str !== '00000000-0000-0000-0000-000000000000' &&
            str.match(/^(\{{0,1}([0-9a-fA-F]){8}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){12}\}{0,1})$/) !== null;
        },

        /// <summary>Responsible for cleaning the uploading file's name.</summary>
        /// <param name="" type=""></param>
        /// <returns type=""></returns>
        cleanFileName: function (fileName) {

            if (fileName === false || fileName === undefined || fileName.length <= 0) {
                window.console.log("Argument:'fileName' must be set.");
                return "";
            }

            var dot = fileName.lastIndexOf('.');
            var extension = fileName.substr(dot, fileName.length);
            fileName = fileName.substr(0, dot);

            // This characters are invalid in Windows filenames \/:"*?<>|, so cleaning it
            var illegalChars = /[\/:"*?<>|�`^&,~';]/g;
            var cleanFileName = fileName.replace(illegalChars, "");

            // Just being safe, if filename consist only the illegal characters then filename should be renamed as file 
            if (cleanFileName.length <= 0) {
                cleanFileName = "file";
            }

            return cleanFileName + extension;
        },

        /// <summary>
        ///     Only allows numbers and deimal point to be entered into a input box,
        ///     add it to a keypress event on a textbox
        /// </summary>
        /// <param name="" type=""></param>
        /// <returns type=""></returns>
        numberOnly: function (items) {

            for (var i = 0; i < items.length; i++) {
                var el = items[i];
                el.onkeypress = function (e) {
                    var charCode = event.which || event.keyCode;
                    return [49, 50, 51, 52, 53, 54, 55, 56, 57, 48, 37, 46, 8].indexOf(charCode) > -1;
                };
            }
        },

        /*
         * 
         */
        currentDate: function () {
            var d = new Date();
            return (d.getFullYear() + "/" + (d.getMonth()+1) + "/" + d.getDate());
        },

        /*
         *
         */
        getGuid: function() {

            function generate() {
                return Math.floor((1 + Math.random()) * 0x10000)
                    .toString(16)
                    .substring(1);
            }

            return generate() + generate() + '-' + generate() + '-' + generate() + '-' +
                generate() + '-' + generate() + generate() + generate();
        },

        /*
         *
         */
        getBaseUrl: function() {
            var pathArray = window.location.href.split( '/'),
                protocol = pathArray[0],
                host = pathArray[2],
                url = protocol + '//' + host;

            return url;
        },

        /*
         *
         */
        getDeviceType: function () {

            var navigator = window.navigator,
                userAgent = navigator.userAgent;

            return {

                isIpod: (/ipod/i).test(userAgent),
                isIPhone: (/iphone/i).test(userAgent),
                isIPad: (/ipad/i).test(userAgent),
                isAndroid: (/android/i).test(userAgent),
                isMac: (/mac/i).test(navigator.platform),
                isChromebook: (/CrOS/).test(userAgent)
            }
        }
    };
}();