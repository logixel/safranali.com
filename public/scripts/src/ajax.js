
/*
 * Source: http://api.jquery.com/jQuery.ajax/
 */
kore.ajax = function () {
    "use strict";

    var _contentType = "application/json; charset=utf-8",
        _json = kore.json,
        _isNull = kore.isNull,

        /*
         * 
         */
        _setContentType = function(contentType) {

            if(_isNull(contentType) === false) {
                _contentType = contentType;
            }
        },

        /*
         * <summary>For making get request to server</summary>
         * <param name="requestType" type="string">For specifying the request type, is it a GET or POST.</param>
         * <param name="dataType" type="string">The type of data that you're expecting back from the server [default is json].</param>dataType = dataType.toLowerCase() || "json";
         * <param name="model" type="object">The object making the request.</param>
         * <param name="url" type="string">A string containing the URL to which the request is sent.</param>
         * <param name="args" type="object">Data to be sent to the server. It is converted to a query string, if not already a string.</param>
         * <param name="callback" type="function">
         *     A function to call when request is completed, passes back the model = {Success, Status, Caller, Url, args, Data, JqXHR, Exception}
         *     <field name="success" type="bool">State result showing request was successful or not</field>
         *     <field name="status" type="string">A string explaining current state of the request</field>
         *     <field name="caller" type="object">An object who made the request</field>
         *     <field name="url" type="string">A url to which requests was made</field>
         *     <field name="args" type="string">Parameters sent with the request.</field>
         *     <field name="data" type="object">Returns data (if any) from the server when request is successful.</field>
         *     <field name="jqXHR" type="object">The jQuery XMLHttpRequest (jqXHR) object</field>
         * </param>
         */
        _request = function(requestType, dataType, model, url, args, callback) {

            // standard model object passed between requests and callbacks
            var _model = {
                    success: false,
                    status: "",
                    model: model,
                    url: url,
                    args: args,
                    data: {},
                    jqXHR: {}
                };

            // check for null, if not convert json data object to json string
            if (args != null) {
                // we add a timestamp to the POST to stop iphone 5 from caching the response
                args = _json.stringify(args);
            }

            // Perform an asynchronous HTTP (Ajax) request
            $.ajax({
                type: requestType,
                url: url,
                contentType: _contentType,
                dataType: dataType,
                data: args
            })
            .done(function(data, textStatus, jqXHR) {

                _model.success = true;
                _model.data = _json.parse(data);
                _model.status = textStatus;
                _model.jqXHR = jqXHR;
            })
            .fail(function(jqXHR, textStatus, exception) {

                    _model.status = textStatus;
                    _model.jqXHR = jqXHR;
                    _model.exception = exception;
            })
            .always(function() {

                _setContentType("application/json; charset=utf-8");

                if (_isNull(callback) === false) {
                    callback(_model);
                }
            });

            return this;
        },

        /*
         * <summary>For making get request to server</summary>
         * <param name="dataType" type="string">
         *     The type of data that you're expecting back from the server [default is json].
         *     Other dataTypes are xml/html/jsonp/text/script
         * </param>
         * <param name="model" type="object">The object making the request.</param>
         * <param name="url" type="string">A string containing the URL to which the request is sent.</param>
         * <param name="args" type="object">Data to be sent to the server. It is converted to a query string, if not already a string.</param>
         * <param name="callback" type="function">A function to call when request is completed.</param>
         */
        _get = function (dataType, model, url, args, callback) {

            return _request("GET", dataType, model, url, args, callback);
        },

        /*
         * <summary>For making post request to server</summary>
         * <param name="dataType" type="string">The type of data that you're expecting back from the server [default is json].</param>dataType = dataType.toLowerCase() || "json";
         * <param name="model" type="object">The object making the request.</param>
         * <param name="url" type="string">A string containing the URL to which the request is sent.</param>
         * <param name="args" type="object">Data to be sent to the server. It is converted to a query string, if not already a string.</param>
         * <param name="callback" type="function">A function to call when request is completed.</param>
         */
        _post = function (dataType, model, url, args, callback) {

            return _request("POST", dataType, model, url, args, callback);
        },

        /*
         * <summary>For making delete request to server</summary>
         * <param name="dataType" type="string">The type of data that you're expecting back from the server [default is json].</param>dataType = dataType.toLowerCase() || "json";
         * <param name="model" type="object">The object making the request.</param>
         * <param name="url" type="string">A string containing the URL to which the request is sent.</param>
         * <param name="args" type="object">Data to be sent to the server. It is converted to a query string, if not already a string.</param>
         * <param name="callback" type="function">A function to call when request is completed.</param>
         */
        _delete = function (dataType, model, url, args, callback) {

            return _request("DELETE", dataType, model, url, args, callback);
        },

        /*
         * <summary>For making put request to server</summary>
         * <param name="dataType" type="string">The type of data that you're expecting back from the server [default is json].</param>dataType = dataType.toLowerCase() || "json";
         * <param name="model" type="object">The object making the request.</param>
         * <param name="url" type="string">A string containing the URL to which the request is sent.</param>
         * <param name="args" type="object">Data to be sent to the server. It is converted to a query string, if not already a string.</param>
         * <param name="callback" type="function">A function to call when request is completed.</param>
         */
        _put = function (dataType, model, url, args, callback) {

            return _request("PUT", dataType, model, url, args, callback);
        };

    return {

        setContentType: _setContentType,
        get: _get,
        post: _post,
        delete: _delete,
        put: _put
        
    };
}();
