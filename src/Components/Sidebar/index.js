import { useState } from "react";

function Sidebar() {
    const [isActive, setActive] = useState('');

    const toggleClass = (section) => {
        setActive(section);
    };

    return (
        <div id="header" class="skel-layers-fixed">

            <div class="top">

                {/*-- Logo --*/}
                <div id="logo">
                    <span class="image avatar48"><img src="images/avatar.jpg" alt="avatar" /></span>
                    <h1 id="title">Safran Ali</h1>
                    <p>Lead Software Engineer</p>
                </div>
                {/*-- /Logo --*/}

                {/*-- Nav --*/}
                <nav id="nav">
                    <ul>
                        <li><a href="#about" id="about-link" class="skel-layers-ignoreHref" className={isActive === 'about' ? 'active': null} onClick={() => toggleClass('about')}><span class="icon fa-user">About Me</span></a></li>
                        <li><a href="#skills" id="skills-link" class="skel-layers-ignoreHref" className={isActive === 'skills' ? 'active': null} onClick={() => toggleClass('skills')}><span class="icon fa-laptop">Technical Skills</span></a></li>
                        <li><a href="#experience" id="experience-link" class="skel-layers-ignoreHref" className={isActive === 'experience' ? 'active': null} onClick={() => toggleClass('experience')}><span class="icon fa-briefcase">Work Experience</span></a></li>
                        <li><a href="#education" id="education-link" class="skel-layers-ignoreHref" className={isActive === 'education' ? 'active': null} onClick={() => toggleClass('education')}><span class="icon fa-graduation-cap">Education</span></a></li>
                        {/* <li><a href="#certifications" id="certifications-link" class="skel-layers-ignoreHref" className={isActive === 'certifications' ? 'active': null} onClick={() => toggleClass('certifications')}><span class="icon fa-certificate">Certifications</span></a></li> */}
                        <li><a href="#contact" id="contact-link" class="skel-layers-ignoreHref" className={isActive === 'contact' ? 'active': null} onClick={() => toggleClass('contact')}><span class="icon fa-envelope">Contact</span></a></li>
                    </ul>
                </nav>
                {/*-- /Nav --*/}
            </div>

            <div class="bottom">

            {/*-- Social --*/}
                <ul class="icons">
                    <li><a href="http://twitter.com/safranali" target="_blank" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
                    <li><a href="http://uk.linkedin.com/in/safranali" target="_blank" class="icon fa-linkedin"><span class="label">LinkedIn</span></a></li>
                    {/*--<li><a href="http://github.com/safranali" target="_blank" class="icon fa-github"><span class="label">Github</span></a></li>
                    <li><a href="#contact" class="icon fa-envelope"><span class="label">Email</span></a></li>--*/}
                </ul>
            {/*-- /Social --*/}
            </div>

        </div>
    );
}

export default Sidebar;