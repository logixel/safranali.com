
function Footer () {
    return (
        <div id="footer">
            {/*-- Copyright --*/}
            <ul class="copyright">
                <li>Safran Ali &copy; 2024</li>
            </ul>
            {/*--<ul class="copyright">
                <li><a href="http://twitter.com/safranali" target="_blank"><span class="fa fa-twitter"></span></a></li>
                <li><a href="http://uk.linkedin.com/in/safranali" target="_blank"><span class="fa fa-linkedin"></span></a></li>
                <li><a href="http://github.com/safranali" target="_blank"><span class="fa fa-github"></span></a></li>
            </ul>--*/}
        </div>
    );
}

export default Footer;