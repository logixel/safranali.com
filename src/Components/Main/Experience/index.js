
function Experience() {
    return (
        <section id="experience" class="three">
            <div class="container">
                <header>
                <h2>Work Experience</h2>
                </header>

                <div class="experience">
                <h3>Lead Developer</h3>
                <h4><a href="#" target="_blank">Betway Group</a></h4>
                <ul class="description">
                    <li>Leading the team and its developers to provide business and technical expertise in requirements solicitations, system analysis, technical design, programming, and documentation.</li>
                    <li>Preparing detailed functional specifications and system workflows for applications development and implementation.</li>
                    <li>Supporting, troubleshooting, and maintaining the production systems as required, optimizing performance and cost management of the apps in Azure cloud.</li>
                    <li>Working with stakeholders and driving software solutions to completion on time while providing regular status updates.</li>
                    <li>Overseeing product delivery and major initiatives, recruitment and resource planning, team development and its technical progression.</li>
                </ul>
                <div class="row">
                <ul class="6u">
                    <li><strong>Location:</strong> London, UK</li>
                    <li><strong>Period:</strong> Mar, 2021 - Present</li>
                    <li><strong>Job type:</strong> Full-time</li>
                    <li><strong>Skills gained:</strong> Agile development, project/product delivery, technical leadership, resource planning, people management and progression, mentoring, and recruitment.</li>
                </ul>
                <ul class="6u">
                    <li><strong>Technologies:</strong> .Net Core, ASP.Net MVC, C#, Javascript, HTML5, React, Next.js, Redis, Powershell, Google Analytics</li>
                    <li><strong>Cloud:</strong> Microsoft Azure</li>
                    <li><strong>Project, process & pipelines:</strong> Azure DevOps, YAML, Terraform, JIRA, Confluence</li>
                    <li><strong>Monitoring:</strong> Application Insight, Elastic Search</li>
                    <li><strong>Testing:</strong> Postman, Playwright, Jasmine</li>
                    <li><strong>Web server:</strong> IIS</li>
                </ul>
                </div>
                </div>

                <div class="experience">
                <h3>Specialist Software Developer</h3>
                <h4><a href="#" target="_blank">Betway Group</a></h4>
                <ul class="description">
                    <li>Taking a lead on technical planning, identifying areas for improvements and implementing new innovations.</li>
                    <li>Application development using .Net Core, ASP.NET MVC, Umbraco, Vanilla JavaScript, and HTML5.</li>
                    <li>Overseeing product development and delivery, management and major initiatives for the new features.</li>
                    <li>Planning and configuring CI/CD pipelines in Azure DevOps deploying to Azure Cloud using YAML and Terraform.</li>
                    <li>Recruitment, mentoring and development of team members.</li>
                </ul>
                <div class="row">
                <ul class="6u">
                    <li><strong>Location:</strong> London, UK</li>
                    <li><strong>Period:</strong> Aug, 2019 - Feb, 2021</li>
                    <li><strong>Job type:</strong> Full-time</li>
                    <li><strong>Skills gained:</strong> Agile development, technical planning and delivery, mentoring and team leadership, recruitment documentation, Azure cloud development, and migration.</li>
                </ul>
                <ul class="6u">
                    <li><strong>Technologies:</strong> .Net Core, ASP.Net MVC, C#, Javascript, HTML5, JSON, Redis, Powershell, Google Analytics</li>
                    <li><strong>Cloud:</strong> Microsoft Azure</li>
                    <li><strong>Project, process & pipelines:</strong> Azure DevOps, YAML, Terraform, JIRA, Confluence</li>
                    <li><strong>Monitoring:</strong> Application Insight, Elastic Search</li>
                    <li><strong>Testing:</strong> Postman</li>
                    <li><strong>Web server:</strong> IIS</li>
                    <li><strong>CMS:</strong> Umbraco</li>
                </ul>
                </div>
                </div>

                <div class="experience">
                <h3>Senior Software Developer</h3>
                <h4><a href="#" target="_blank">Betway Group</a></h4>
                <ul class="description">
                    <li>Web application development using .NET framework, .Net Core, ASP.NET MVC, Umbraco, Vanilla JavaScript, AngularJs, HTML5, CSS3, SASS, GruntJs and Bower.</li>
                    <li>Migrating existing applications to .Net Core with cloud-first development.</li>
                    <li>Moving on-prem applications to Azure Portal with Azure AD integration.</li>
                    <li>Monitoring and reporting of existing/new application and migrated applications using Kibana and Azure Application Insights.</li>
                </ul>
                <div class="row">
                <ul class="6u">
                    <li><strong>Location:</strong> London, UK</li>
                    <li><strong>Period:</strong> Jan, 2018 - Jul, 2019</li>
                    <li><strong>Job type:</strong> Full-time</li>
                    <li><strong>Skills gained:</strong> Project planning and delivery, technical process and policies development, mentoring and team leadership.</li>
                </ul>
                <ul class="6u">
                    <li><strong>Technologies:</strong> .Net Core, ASP.Net MVC, C#, Javascript, HTML5, JSON, Redis, Kibana, Google Analytics</li>
                    <li><strong>Cloud:</strong> Microsoft Azure</li>
                    <li><strong>Project, process & pipelines:</strong> Azure DevOps, YAML, JIRA, Confluence</li>
                    <li><strong>Monitoring:</strong> Elastic Search</li>
                    <li><strong>Testing:</strong> Postman</li>
                    <li><strong>Web server:</strong> IIS</li>
                    <li><strong>CMS:</strong> Umbraco</li>
                </ul>
                </div>
                </div>

                <div class="experience">
                <h3>Web Developer</h3>
                <h4><a href="#" target="_blank">Betway Group</a></h4>
                <ul class="description">
                    <li>Web application development and maintenance using current web standard and responsive/adaptive design.</li>
                    <li>Application development using .NET framework with cutting edge ASP.NET MVC, Umbraco, Vanilla JavaScript, AngularJs, HTML5, CSS3, SASS, GruntJs and Jasmine.</li>
                    <li>Implementation of object-oriented JavaScript with an emphasis on performance, scalability, reusability with a positive user experience.</li>
                    <li>Refine and iterate user experience by designing and implementing new modules and interfaces in complex responsive web applications.</li>
                    <li>Development of web application using BDD, Scrum and Agile methodologies.</li>
                </ul>
                <div class="row">
                <ul class="6u">
                    <li><strong>Location:</strong> London, UK</li>
                    <li><strong>Period:</strong> Jan, 2016 - Dec, 2017</li>
                    <li><strong>Job type:</strong> Full-time</li>
                    <li><strong>Skills gained:</strong> organizational policies and procedures, working under pressure, teamwork, time management, scrum, agile and progressive development.</li>
                </ul>
                <ul class="6u">
                    <li><strong>Technologies:</strong> ASP.Net MVC, C#, .Net Framework, Javascript, AngularJs, AJAX, JSON, CSS/CSS3, SASS, Ms Office, Google Analytics, Adobe Photoshop</li>
                    <li><strong>Testing:</strong>  Jasmine</li>
                    <li><strong>Version Control:</strong> TFS &amp; Git</li>
                    <li><strong>Web server:</strong> IIS</li>
                    <li><strong>CMS:</strong> Umbraco</li>
                </ul>
                </div>
                </div>

                <div class="experience">
                <h3>Web Developer</h3>
                <h4><a href="#" target="_blank">London Bridge Media Technology</a></h4>
                <ul class="description">
                    <li>Web application development and maintenance using current web standard and responsive/adaptive design.</li>
                    <li>Application development using .NET framework with cutting edge ASP.NET MVC, Umbraco, Vanilla JavaScript, AngularJs, HTML5, CSS3, SASS, GruntJs and Jasmine.</li>
                    <li>Implementation of object-oriented JavaScript with an emphasis on performance, scalability, reusability with a positive user experience.</li>
                    <li>Refine and iterate user experience by designing and implementing new modules and interfaces in complex responsive web applications.</li>
                    <li>Development of web application using BDD, Scrum and Agile methodologies.</li>
                </ul>
                <div class="row">
                <ul class="6u">
                    <li><strong>Location:</strong> London, UK</li>
                    <li><strong>Period:</strong> Jun, 2014 - Dec, 2015</li>
                    <li><strong>Job type:</strong> Full-time</li>
                    <li><strong>Skills gained:</strong> organizational policies and procedures, working under pressure, teamwork, time management, scrum, agile and progressive development.</li>
                </ul>
                <ul class="6u">
                    <li><strong>Technologies:</strong> ASP.Net MVC, C#, .Net Framework, Javascript, AngularJs, AJAX, JSON, CSS/CSS3, SASS, Ms Office, Google Analytics, Adobe Photoshop</li>
                    <li><strong>Testing:</strong>  Jasmine</li>
                    <li><strong>Version Control:</strong> TFS &amp; Git</li>
                    <li><strong>Web server:</strong> IIS</li>
                    <li><strong>CMS:</strong> Umbraco</li>
                </ul>
                </div>
                </div>

                <div class="experience">
                <h3>Mid - Senior Developer</h3>
                <h4><a href="http://www.adgistics.com" target="_blank">Adgistics Ltd.</a></h4>
                <ul class="description">
                    <li>Brand center development for managing digital assets for publishing and production.</li>
                    <li>Development of web application as per client standard and features, and also monitoring web server and sites technical performance.</li>
                    <li>Assesment of time and resources for new functionalities.</li>
                    <li>Designing relational database systems and integrating them with projects.</li>
                </ul>
                <div class="row">
                    <ul class="6u">
                    <li><strong>Location:</strong> London, UK</li>
                    <li><strong>Period:</strong> Dec, 2012 - Jun, 2014</li>
                    <li><strong>Job type:</strong> Full-time</li>
                    <li><strong>Skills gained:</strong> brand management, organizational policies and procedures, project development, working under pressure, teamwork, time management, user training and customer relations.</li>
                    </ul>
                    <ul class="6u">
                    <li><strong>Technologies:</strong> ASP.Net MVC, C#, .Net Framework, Javascript, jQuery, AJAX, JSON, CSS/CSS3, SASS, SQL Server, NHibernate, Ms Office, PIWIK, Adobe Photoshop and Gimp</li>
                    <li><strong>Testing:</strong>  NUnit, JustMock &amp; Jasmine</li>
                    <li><strong>Version Control:</strong> Mercurial &amp; Git</li>
                    <li><strong>Web server:</strong> IIS &amp; Apache</li>
                    <li><strong>CMS:</strong> Composite C1</li>
                    </ul>
                </div>
                </div>

                <div class="experience">
                <h3>Web Developer</h3>
                <h4><a href="#" target="_blank">Freelance</a></h4>
                <ul class="description">
                    <li>Developed and implemented a medical drugs inventory and management system.</li>
                    <li>Development of business prototype and database schema with end-user reports generation.</li>
                    <li>Supervision of projects with on-site user training, support, and delivering of technical and user manual.</li>
                </ul>
                <div class="row">
                    <ul class="6u">
                    <li><strong>Location:</strong> London, UK</li>
                    <li><strong>Period:</strong> Mar, 2010 - Jun, 2014</li>
                    <li><strong>Job type:</strong> Contractual</li>
                    <li><strong>Skills gained:</strong> project development and supervision, customer relations, user training, scrum, kanban, agile and progressive development.</li>
                    </ul>
                    <ul class="6u">
                    <li><strong>Technologies:</strong> ASP.Net MVC, C#, .Net Framework, Javascript, jQuery, AJAX, JSON</li>
                    <li><strong>Web design:</strong> CSS/CSS3, SASS, LESS</li>
                    <li><strong>Database:</strong> SQL Server, MySQL, NHibernate</li>
                    <li><strong>Testing:</strong>  NUnit, JustMock &amp; Jasmine</li>
                    <li><strong>Version Control:</strong> Mercurial, Git &amp; TFS</li>
                    <li><strong>Build:</strong> GruntJs, Jenkins</li>
                    <li><strong>Web server:</strong> IIS &amp; Apache</li>
                    <li><strong>CMS:</strong> Composite C1, Umbraco</li>
                    <li><strong>Analytics:</strong> Google, PIWIK</li>
                    <li><strong>Social media integration:</strong> Twitter, Facebook, Instagram &amp; Tumblr</li>
                    <li><strong>API integration:</strong> Parse &amp; Mandrill</li>
                    <li><strong>Graphics:</strong> Adobe Photoshop &amp; Gimp</li>
                    <li><strong>OS:</strong> Windows, MAC OS, Ubuntu</li>
                    <li><strong>Application:</strong> Ms Office, Dreamweaver, Outlook</li>
                    {/* <li><strong>References:</strong> N/A</li> */}
                    </ul>
                </div>
                </div>

                <div class="experience">
                <h3>Software Developer</h3>
                <h4><a href="http://webcandy.org" target="_blank">Webcandy Ltd.</a></h4>
                <ul class="description">
                    <li>Development of web page infrastructure and application with more advanced graphics and features, and also monitoring web server and sites technical performance.</li>
                    <li>Assisting in the planning of strategies regarding web development and search engine optimization.</li>
                    <li>Designing relational database systems and integrating them with projects.</li>
                    <li>Social media integration (i.e. Facbook, Twitter, Instagram and Tumblr) in to web apps.</li>
                </ul>
                <div class="row">
                    <ul class="6u">
                    <li><strong>Location:</strong> Eversley, UK</li>
                    <li><strong>Period:</strong> Jan, 2011 - Dec, 2012</li>
                    <li><strong>Job type:</strong> Full-time</li>
                    <li><strong>Skills gained:</strong> project development and management, working under pressure, teamwork, time management and customer relations.</li>
                    </ul>
                    <ul class="6u">
                    <li><strong>Technologies:</strong> ASP.Net, C#, .Net Framework 3.5, AJAX, JSON, JQuery, CSS, LESS, SQL Server, VBA (Excel Programming)</li>
                    <li><strong>Web server:</strong> IIS</li>
                    <li><strong>Application:</strong> Ms Office, Dreamweaver, Outlook</li>
                    <li><strong>Analytics:</strong> Google Analytics</li>
                    <li><strong>Social media integration:</strong> Facebook, Twitter, Youtube</li>
                    <li><strong>OS:</strong> Windows, MAC OS</li>
                    </ul>
                </div>
                </div>

                <div class="experience">
                <h3>Programmer Analyst</h3>
                <h4><a href="http://www.aku.edu" target="_blank">Aga Khan University</a></h4>
                <ul class="description">
                    <li>Developed and implemented a medical drugs inventory and management system.</li>
                    <li>Development of business prototype and database schema with end-user reports generation.</li>
                    <li>Supervision of projects with on-site user training, support, and delivering of technical and user manual.</li>
                </ul>
                <div class="row">
                    <ul class="6u">
                    <li><strong>Location:</strong> Karachi, Pakistan</li>
                    <li><strong>Period:</strong> Feb, 2007 - Aug, 2008</li>
                    <li><strong>Job type:</strong> Full-time</li>
                    <li><strong>Skills gained:</strong> organizational policies and procedures, independent project development and management, working under pressure, teamwork, user training and customer relations.</li>
                    </ul>
                    <ul class="6u">
                    <li><strong>Technologies:</strong> .Net Framework 2.0, ASP/ASP.Net, C#</li>
                    <li><strong>Database:</strong> SQL Server 2000/2005</li>
                    </ul>
                </div>
                </div>
            </div>
        </section>
    );
}

export default Experience;