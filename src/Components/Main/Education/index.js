
function Education() {
    return (
        <section id="education" class="two">
            <div class="container">
                <header>
                    <h2>Education</h2>
                </header>

                <div>
                    <h3><a href="http://www.ed.ac.uk/" target="_blank">University of Edinburgh</a></h3>
                    <h4>MSc. in Computer Science</h4>
                    <h5>Software Engineering</h5>
                    <span>Sep, 2008 - Aug, 2009</span>
                </div>
                <br />
                
                <div>
                    <h3><a href="http://www.ssuet.edu.pk/" target="_blank">Sir Syed University of Engineering and Technology</a></h3>
                    <h4>BSc. in Computer Engineering</h4>
                    <h5>Software Engineering</h5>
                    <span>Jan, 2003 - Dec, 2006</span>
                </div>
            </div>
        </section>
    );
}

export default Education;