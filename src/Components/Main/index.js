import About from "./About";
import Skills from "./Skills";
import Experience from "./Experience";
import Education from "./Education";
import Certifications from "./Certifications";
import Contact from "./Contact";

function Main () {
    return (
        <div id="main">

            {/*-- About --*/}
            <About />
            {/*-- /About --*/}
            
            {/*-- Skills --*/}
            <Skills />
            {/*-- /Skills --*/}

            {/*-- Experience --*/}
            <Experience />
            {/*-- /Experience --*/}

            {/*-- Education --*/}
            <Education />
            {/*-- /Education --*/}

            {/*-- Certifications --*/}
            {/* <Certifications /> */}
            {/*-- /Certifications --*/}

            {/*-- Contact --*/}
            <Contact />
            {/*-- /Contact --*/}

        </div>
    );
}

export default Main;