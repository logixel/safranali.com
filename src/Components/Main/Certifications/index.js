
function Certifications() {
    return (
        <section id="certifications" class="three">
            <div class="container">

                <header>
                <h2>Certifications</h2>
                </header>
                <div>
                <h3><a href="http://umbraco.com/certified-partners/find-a-certified-developer?group=4.2" target="_blank">Umbraco</a></h3>
                <div style={{ marginTop: '20px' }}><img style={{ width: '30%' }} src="./images/badges/UmbracoCertifiedProfessionalLogo.png" alt="Umbraco Certified Developer" /></div>
                <span>May, 2015</span>
                </div>
            </div>
        </section>
    );
}

export default Certifications;