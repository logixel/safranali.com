
function About() {
    return (
        <section id="about" class="one dark cover">
            <div class="container">

                <header>
                    <h2 class="alt">Hi! I'm <strong>Safran Ali</strong>, a <a>Lead Software Engineer</a> living in <a>London</a>.</h2>
                    <p>I am a technical leader and highly skilled full-stack developer with more than 10 years of well-rounded experience in technologies and tools like HTML5, JavaScript, .Net Core, ASP.Net MVC, C#, Microsoft Azure, Azure DevOps, etc. I am also experienced in using Agile methodology while collaborating with all members of the organization to achieve team and business objectives. I’m capable of managing projects from concept to completion with remarkable deadline sensitivity and strong analytical skills.</p>
                </header>

                {/*<footer>
                    <a href="#portfolio" class="button scrolly">Magna Aliquam</a>
                </footer>*/}

            </div>
        </section>
    );
}

export default About;