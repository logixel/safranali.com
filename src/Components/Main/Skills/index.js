import Skill from "./skill";

function Skills() {
    return (
        <section id="skills" class="two">
            <div class="container">
                <header>
                    <h2>Technical Skills</h2>
                </header>

                <div class="row">
                    <div class="6u">
                        <Skill name={'Javascript'} class={''} rating={'Excellent'} score={'100'} />
                        <Skill name={'jQuery'} class={'mt20'} rating={'Excellent'} score={'100'} />
                        <Skill name={'Html / Html5'} class={'mt20'} rating={'Excellent'} score={'100'} />
                        <Skill name={'GruntJs'} class={''} rating={'Excellent'} score={'100'} />
                        <Skill name={'ASP.Net'} class={'mt20'} rating={'Excellent'} score={'100'} />
                        <Skill name={'OOP'} class={'mt20'} rating={'Excellent'} score={'100'} />
                        <Skill name={'OS (Windows, Mac OS,; Unix)'} class={'mt20'} rating={'Excellent'} score={'100'} />
                        <Skill name={'Version Control (Git, TFS)'} class={'mt20'} rating={'Excellent'} score={'100'} />
                        <Skill name={'Testing (Jest, Jasmine)'} class={'mt20'} rating={'Good'} score={'80'} />
                        <Skill name={'Browser Analytics (Google Analytics, PIWIK)'} class={'mt20'} rating={'Good'} score={'80'} />
                        <Skill name={'API Integration (Parse, Mandrill)'} class={'mt20'} rating={'Good'} score={'80'} />
                    </div>

                    <div class="6u">
                        <Skill name={'C#'} class={'mt20'} rating={'Excellent'} score={'100'} />
                        <Skill name={'Azure Cloud'} class={'mt20'} rating={'Excellent'} score={'100'} />
                        <Skill name={'Azure DevOps'} class={'mt20'} rating={'Excellent'} score={'100'} />
                        <Skill name={'Data Science (Application Insights, Elastic Search)'} class={'mt20'} rating={'Excellent'} score={'100'} />
                        <Skill name={'AJAX, JSON'} class={'mt20'} rating={'Excellent'} score={'100'} />
                        <Skill name={'Ms Office'} class={'mt20'} rating={'Excellent'} score={'100'} />
                        <Skill name={'IIS / Apache'} class={''} rating={'Excellent'} score={'100'} />
                        <Skill name={'CMS (Umbraco)'} class={'mt20'} rating={'Good'} score={'80'} />
                        <Skill name={'CSS3 / SASS/ LESS'} class={'mt20'} rating={'Good'} score={'80'} />
                        <Skill name={'SQL Server'} class={'mt20'} rating={'Good'} score={'60'} />
                        <Skill name={'React Js'} class={''} rating={'Low'} score={'40'} />
                    </div>
                </div>

            </div>
        </section>
    );
}

export default Skills;