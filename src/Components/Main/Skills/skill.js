
function Skill(props) {
    return (
        <div className={'skill ' + props.class}>
            <h3>{props.name}</h3>
            <div class="skillbar-val">
                <span class="low">Low</span>
                <span class="middle">Medium</span>
                <span class="high">High</span>
            </div>
            <div class="skillbar" title={props.rating}>
                <div class="100" className={'skillbarfill skill' + props.score}>{props.rating}</div>
            </div>
        </div>
    );
}

export default Skill;