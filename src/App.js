// import logo from './logo.svg';
import Footer from "./Components/Footer";
import Main from "./Components/Main";
import Sidebar from "./Components/Sidebar";

function App() {
  return (
    <div className="App">
      {/*-- Sidebar --*/}
      <Sidebar />
      {/*-- /Sidebar --*/}

      {/*-- Main --*/}
      <Main />
      {/*-- /Main --*/}

      {/*-- Footer --*/}
      <Footer />
      {/*-- /Footer --*/}
    </div>
  );
}

export default App;
